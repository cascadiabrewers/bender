if (!process.env.SLACK_TOKEN) {
    console.log('Error: Specify token in environment');
    process.exit(1);
}

var Botkit = require('botkit');
var mathjs = require("mathjs");

var controller = Botkit.slackbot({
 debug: false,
 retry: 100,
});

controller.spawn({
  token: process.env.SLACK_TOKEN
}).startRTM(function(err) {
  if (err) {
    throw new Error(err);
  }
});

var enterReplies = ['Hi', 'Target Acquired', 'Hello meatbag.', 'I see you','Kill all humans...']

controller.on(['user_channel_join', 'user_group_join'], function(bot, message) {
  bot.reply(message, enterReplies[Math.floor(Math.random()*enterReplies.length)])
});

controller.hears(["hello"], ['direct_message,direct_mention'], function(bot, message){
  bot.reply(message, enterReplies[Math.floor(Math.random()*enterReplies.length)])
});

var neats=['https://i.imgur.com/i4IdAIQ.gif', 'https://i.imgur.com/DXkLvT2.gif', 'https://i.imgur.com/qGoUx8s.gif', 'https://i.imgur.com/KHenbi1.gif']

controller.hears(["\bneat\b"], ['ambient', 'message_received'], function(bot, message){
  console.log("neat");
  bot.reply(message, neats[Math.floor(Math.random()*neats.length)])
});

controller.hears(["abv"], ["direct_mention", "direct_message"], function(bot, message){
  bot.startConversation(message, askOg);
});

askOg = function(response, convo) {
  convo.ask("What was your OG?", function(response, convo) {
    askFg(response, convo);
    convo.next();
  });
}

askFg = function(response, convo) {
  convo.ask("Ok, and what was your FG?", function(response, convo) {
    convo.say("Thanks.");
    var og = parseFloat(convo.responses["What was your OG?"].text);
    var fg = parseFloat(response.text);
    var re = 0;
    var ogp = 0;
    var fgp = 0;
    var rep = 0;
    var etohwt = 0;
    var etohvol = 0;
    var kcals = 0;

    ogp = (-0.003829+Math.sqrt(Math.pow(0.003829, 2)-4*0.00001572*(1-og)))/(2*0.00001572);
    fgp = (-0.003829+Math.sqrt(Math.pow(0.003829, 2)-4*0.00001572*(1-fg)))/(2*0.00001572);
    rep = fgp*0.8192+ogp*0.1808;
    re = 1+0.003829*rep+0.00001572*Math.pow(rep, 2);
    etohwt = (ogp-rep)/(2.0665-0.010665*ogp);
    etohvol = etohwt*1.25;
    kcals = (6.9*etohwt+4*(rep-0.1))*3.55*fg;
    convo.say("Based on that, the ABV of your beer is " + Math.round(etohvol*100)/100 + "%");
    convo.next();
  });
}



// for the Lagoon health check to not cause infinite restarts, need to have at least something come back on port 3000
controller.setupWebserver(process.env.PORT || 3000, function(err, webserver) {
});
